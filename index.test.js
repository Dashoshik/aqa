const { Builder, By, until } = require('selenium-webdriver'); //подключаем методы Builder, By, until из селениум вебдрайвера
require('selenium-webdriver/chrome');//подключаем библиотеку только те методы, что будут работать с браузером хром
require('chromedriver');//это сервак

const rootURL = 'https://www.mozilla.org/en-US/';
const waitUntilTime = 20000; //это наш таймаут в милисекундах = 20 сек
let driver, el, actual, expected; //объявялем переменные, чтобы объявить их глобально, чтобы они были доступны во всех функциях

describe('suite', () => {
beforeAll(async () => {
    driver = new Builder().forBrowser('chrome').build();//запускаем окно браузера
    await driver.get(rootURL);//ждем окончания предыдущего шага , берем из переменной адрес страницы и наша страничка открывается
}, 10000);
afterAll(async () => {
    await driver.quit();
}, 15000);


async function getElementByXPath(xpath) { //получаем элемент по xpath
    const el = await driver.wait(until.elementLocated(By.xpath(xpath)), waitUntilTime);//различные wait для ожидания , 
    //что элемент появится на странице 
    //и что будет видимый
    return await driver.wait(until.elementIsVisible(el), waitUntilTime);
};


    it('should click on navbar button to display a drawer', async () => {
        el = await getElementByXPath("//*[@class='c-primary-cta-title']")//вызываем функцию и передаем в качестве аргумента xpath
        //и записываем в переменную el результат функции 

        actual = await el.getText();//в нашу глобальную переменную записываем значение текста элемента, используем метод получить текст
        expected = 'Take back your privacy';//в глоб переменную записываем строку
        expect(actual).toEqual(expected);//используем метод toEqual для объекта expect для проверки равенства значений переменных actual и expected
    });
});

//залили на гит
