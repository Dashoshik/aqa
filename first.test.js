const { Builder, By, Key, until } = require('selenium-webdriver'); //подключаем методы Builder, By, until из селениум вебдрайвера
require('selenium-webdriver/chrome');//подключаем библиотеку только те методы, что будут работать с браузером хром
require('chromedriver');//это сервак

const waitUntilTime = 20000; //это наш таймаут в милисекундах = 20 сек
let driver;

describe ('Google search',()=>{//describe принимает два аргумента первый это название, а второй это функция, которых омжет быть несколько
    beforeEach(async()=>{
        driver = new Builder().forBrowser('chrome').build();
        await (await driver).get('https://google.com');
    }, 10000);

    afterEach(async()=>{
        await driver.quit();
    }, 1000);
    async function getElementByXPath(xpath){
        const el = await driver.wait(until.elementLocated(By.xpath(xpath)), waitUntilTime);
        return await driver.wait(until.elementIsVisible(el),waitUntilTime);
    };
    it('Find results for selenium-webdriverjs in google', async()=>{
        let element = await driver.findElement({name:'q'});//ищем элемент на странице гугл и записываем найденный элемент в переменную
        await element.clear();//в найденном элементе в гугле очищаем поле вдруг там автозаполнение
        await element.sendKeys('selenium-webdriverjs').submit;//вставляем в поле фразу и выполняем метод сабмит
        
        await driver.findElement({name:'q'}).sendKeys('selenium-webdriverjs').submit;//эта строка идентична предыдущим трем, найти элемент, вставить в него строку и выполнить сабмит
        
        await driver.findElement(By.name('q')).sendKeys('selenium-webdriverjs',Key.RETURN); //найти элемент, вставить в него строку и нажать энтер

        await driver.findElement(By.xpath("//*[@name='q']")).sendKeys('selenium-webdriverjs');//через xrath находим элемент и вставляем туда текст

        await driver.wait(until.elementIsVisible(driver.findElement({path:"//*[@name='btnK']"})),1000).click();//найти элемент кнопки "Поиск в GOOGLE" 
        //c ожиданием появляения 1 сек и кликнуть по нему
        await driver.sleep(3000);//установить явный таймаут в 3 сек

        driver.wait(until.titleIs('selenium-webdriverjs - Поиску в Google'),1000); //ждем пока тайтл страницы не будет selenium-webdriverjs - Поиску в Google, 
        //и проверяет татйтл в течение 1 сек, если в течение секунды тайтл не будет, то тест свалится. Это selenium webdriver библиотека
        expect(await driver.getTitle()).toEqual('selenium-webdriverjs - Поиску в Google');//ожидаем запрашиваем текущий тайтл страницы и 
        //сравниваем соответсвует ли он тому, что мы написали, это jest библиотека

    })

});