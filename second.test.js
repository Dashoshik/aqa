const { Builder, By, Key, until } = require('selenium-webdriver'); //подключаем методы Builder, By, until из селениум вебдрайвера
require('selenium-webdriver/chrome');//подключаем библиотеку только те методы, что будут работать с браузером хром
require('chromedriver');//это сервак

const waitUntilTime = 20000; //это наш таймаут в милисекундах = 20 сек
let driver;

describe ('Hotline project',()=>{//describe принимает два аргумента первый это название, а второй это функция, которых омжет быть несколько
    beforeEach(async()=>{
        driver = new Builder().forBrowser('chrome').build();
        await (await driver).get('https://hotline.ua');
    }, 10000);

    afterEach(async()=>{
        await driver.quit();
    }, 1000);
    async function getElementByXPath(xpath){
        const el = await driver.wait(until.elementLocated(By.xpath(xpath)), waitUntilTime);
        return await driver.wait(until.elementIsVisible(el),waitUntilTime);
    };
    it('Hotline registration form', async()=>{
        await (await getElementByXPath('//*[text()="Вхід"]')).click();
        await (await getElementByXPath('//*[text()="Зареєструватися"]')).click();
        await (await getElementByXPath('//*[@name="email"]')).sendKeys('fkjkdf00j@gmail.com');
        await (await getElementByXPath('//*[@name="login"]')).sendKeys('DevRepublik349df');
        await (await getElementByXPath('//*[@name="password"]')).sendKeys('Priz2018');
        await (await getElementByXPath('//*[@id="submit-button"]')).click();


        let element = await getElementByXPath('//title');
        let actual = await element.getText();
        expect(actual).toEqual('Завершення реєстрації');
    });

});