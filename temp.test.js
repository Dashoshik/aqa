// index.test.js
const { Builder, By, until } = require('selenium-webdriver');
require('selenium-webdriver/chrome');
require('chromedriver');


const rootURL = 'https://www.mozilla.org/en-US/';
const waitUntilTime = 20000;
let driver, el, actual, expected;
// jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 * 5;



describe ('suite', () => {

    beforeAll(async () => {
        driver = new Builder().forBrowser('chrome').build();
        await driver.get(rootURL);
      }, 10000);
     
      afterAll(async () => {
        await driver.quit();
      }, 15000);

    async function getElementById(id) {
        const el = await driver.wait(until.elementLocated(By.id(id)), waitUntilTime);
        return await driver.wait(until.elementIsVisible(el), waitUntilTime);
      };
    async function getElementByXPath(xpath) {
        const el = await driver.wait(until.elementLocated(By.xpath(xpath)), waitUntilTime);
        return await driver.wait(until.elementIsVisible(el), waitUntilTime);
      };
 
  // it('initialises the context', async () => {
  //   await driver.manage().window().setPosition(0, 0)
  //   await driver.manage().window().setSize(1280, 1024)
  //   await driver.get(rootURL)
  // });


  it('should click on navbar button to display a drawer', async () => {
    // el = await getElementById('mzp-c-menu-panel-developers')
    // el.click()
    el = await getElementByXPath('//*[@class="c-primary-cta-title"]')
    
    actual = await el.getText();
    expected = 'Take back your privacy';
    expect(actual).toEqual(expected);
  })
})



